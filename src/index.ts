import program from 'commander'
import app from './server/server'
import NumberConverter from './services/arabicToRoman'
import { readConfigFile } from './lib/reader'
const numberConverter = new NumberConverter()
/**
 * Entry point of the application
 */
program
	.version('0.1.0')
	.option('-a, --api', '==> Launch conversion api')
	.option('-f, --file', '==> Launch conversion script with /config/config.js.number as arg')
	.option('-F, --customFile [value]', '==> Launch conversion script with custom file passed as arg')
	.option('-A, --array [value]', '==> Launch conversion script with an array of numbers passed in command line')
	.option('-c, --commandLine [value]', '==> Launch conversion script with command line arg')
	.parse(process.argv)

function checkAndLog(arg: any): void {
	try {
		const res = Array.isArray(arg) ? arg.map(number => numberConverter.arabicToRoman(number)) : numberConverter.arabicToRoman(arg)
		console.log(res)
	} catch (e) {
		console.error(e.message)
	}
}

if (program.api) {
	try {
		app.listen(app.get('port'), (): void => {
			console.log(`>>  App is running at http://localhost:${app.get('port')} in ${app.get('env')} mode`)
			console.log('>>  Press CTRL-C to stop\n')
		})
	} catch (e) {
		console.error('An error occured with the server')
	}
} else if (program.file || program.customFile) {
	;(async function(): Promise<void> {
		const res: string | null = await readConfigFile(program.customFile).catch(e => {
			console.error(`Can't read the config file: , ${e}`)
		})
		if (res) checkAndLog(res)
	})()
} else if (program.array) {
	try {
		const array = process.argv.slice()
		array.splice(0, 3)
		const arr = JSON.parse(array.join('').trim())
		if (!Array.isArray(arr)) console.error('You should pass an array to array option')
		else checkAndLog(arr)
	} catch (e) {
		console.log(e)
		console.error('Invalid array')
	}
} else if (program.commandLine) {
	try {
		const res: string | null = numberConverter.arabicToRoman(program.commandLine)
		console.log(res)
	} catch (e) {
		console.error(e.message)
	}
} else if (process.argv[2]) {
	console.error('Incorrect argument, please refer to the doc')
} else {
	console.error('You should give an argument, please refer to the doc')
}
