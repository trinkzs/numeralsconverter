import { expect } from 'chai'
import app from '../server/server'
import axios from 'axios'

/**
 * API testing
 */
const numbers: Array<number> = [1, 2, 3, 4, 5, 9, 10]
const badTypes: Array<any> = ['string', {}, [], function(): void {}, -1]
/** Do post request and give result
 *
 * @param number number to give to the api endpoint
 * @param done callback
 */
function useApi(number: number, done: CallableFunction): void {
	axios
		.post('http://localhost:3000/arabicToRoman/', { number })
		.then(res => {
			done(null, res)
		})
		.catch(err => {
			if (err) done(new Error(err))
		})
}

before(function(done) {
	app.listen(app.get('port'), (): void => {
		console.log(`>>  App is running at http://localhost:${app.get('port')} in ${app.get('env')} mode`)
		console.log('>>  Press CTRL-C to stop\n')
		done()
	})
})

describe('Testing API responses', function() {
	// Testing API with good inputs
	for (let i = 0; i < numbers.length; i++) {
		it('Should send a res', function(done) {
			useApi(3, function(err, res) {
				if (err) {
					done(new Error(err))
				} else {
					done()
				}
			})
		})
	}
	// Testing API with bad inputs
	for (let i = 0; i < badTypes.length; i++) {
		it('Should send an error', function(done) {
			useApi(badTypes[i], function(err, res) {
				if (err) {
					done()
				} else {
					done(new Error('Should have give an Error'))
				}
			})
		})
	}
})
