import { expect } from 'chai'
import NumberConverter from '../services/arabicToRoman'
import { Func } from 'mocha'

/**
 * Conversion algo testing
 */
const numberConverter = new NumberConverter()
const numbers: Array<number> = [1, 2, 3, 4, 5, 9, 10, 14, 19, 20, 30, 31, 49, 50, 99, 100, 400, 900, 1000, 1030, 1400, 2000]
const negNumbers: Array<number> = numbers.map(x => x * -1)
const strNumbers: Array<string> = numbers.map(x => x.toString())
const badTypes: Array<any> = ['string', {}, [], function(): void {}]
const answers: Array<string> = ['I', 'II', 'III', 'IV', 'V', 'IX', 'X', 'XIV', 'XIX', 'XX', 'XXX', 'XXXI', 'XLIX', 'L', 'XCIX', 'C', 'CD', 'CM', 'M', 'MXXX', 'MCD', 'MM']

/** Ensure that conversion throws error
 *
 * @param number
 */
function doThrow(number: number): Func {
	return function(): Chai.Assertion {
		return expect(function(): string | null {
			return numberConverter.arabicToRoman(number)
		}).to.throw(Error, '')
	}
}

/** Ensure that conversion doesnt throws error
 *
 * @param number
 */
function dontThrow(number): Func {
	return function(): Chai.Assertion {
		return expect(function(): string | null {
			return numberConverter.arabicToRoman(number)
		}).to.not.throw(Error, '')
	}
}

/** Ensure that conversion algo give the right conversion
 *
 * @param index
 */
function testAnswer(index) {
	return function(): Chai.Assertion {
		return expect(numberConverter.arabicToRoman(numbers[index])).to.equal(answers[index])
	}
}

describe('arabicToRoman function', function() {
	describe('Testing good inputs', function() {
		for (let i = 0; i < numbers.length; i++) {
			it('Accepts numbers', dontThrow(numbers[i]))
			it('Accepts strings number', dontThrow(strNumbers[i]))
		}
	})
	describe('Testing wrong inputs', function() {
		for (let i = 0; i < numbers.length; i++) {
			it('Throws on non-numbers literals', doThrow(badTypes[i]))
			it('Throws on NaN', doThrow(NaN))
			it('Throws on negative numbers', doThrow(negNumbers[i]))
		}
	})
	describe('Testing results', function() {
		for (let i = 0; i < numbers.length; i++) {
			it(`Works for ${numbers[i]} => ${numberConverter.arabicToRoman(numbers[i])}`, testAnswer(i))
		}
	})
})
