import express from 'express'
import bodyparser from 'body-parser'
import NumberConverter from '../services/arabicToRoman'
const numberConverter = new NumberConverter()
const app = express()

/**
 * A simple API with express that listen on port 3000 by default
 * @route POST: /arabicToRoman => Take number as parameter and return roman conversion
 */
app.set('port', process.env.PORT || 3000)
app.use(
	bodyparser.json({
		strict: false,
	})
)
app.post('/arabicToRoman', function(req, res) {
	const number: any = req.body.number
	try {
		const reponse = numberConverter.arabicToRoman(number)
		res.send(reponse)
	} catch (e) {
		res.status(500).send(e.message)
	}
})
export default app
