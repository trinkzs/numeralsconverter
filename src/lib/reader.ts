import fs from 'fs'
import util from 'util'
const readFileP = util.promisify(fs.readFile)

/**
 * Read /src/config/config.json and get number property from this file
 * @return Promise<any>
 */
export async function readConfigFile(path?: string): Promise<any> {
	let config: string
	try {
		config = await readFileP(path || './src/config/config.json', 'utf-8')
		const json = JSON.parse(config)
		return json.number
	} catch (e) {
		throw e
	}
}
