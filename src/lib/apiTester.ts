import axios from 'axios'

/**
 * A simple post request to test the API with axios
 */

/**
 * Simple post that logs the api response or crash
 * @param number the arg we want to pass to the api endpoint
 */
function testApi(number): void {
	axios
		.post('http://localhost:3000/arabicToRoman/', {
			number: {},
		})
		.then(res => {
			console.log(res.data)
		})
}

testApi(100)
