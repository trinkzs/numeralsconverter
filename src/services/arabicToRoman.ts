/**
 *
 */
export default class Converter {
	protected numbKeys

	constructor() {
		this.numbKeys = {
			M: 1000,
			CM: 900,
			D: 500,
			CD: 400,
			C: 100,
			XCIX: 99,
			L: 50,
			XLIX: 49,
			X: 10,
			IX: 9,
			V: 5,
			IV: 4,
			I: 1,
		}
	}

	/** Convert given number to roman conversion
	 *
	 * @param number
	 * @return string Complete information of the cinema or void if error
	 */
	public arabicToRoman(number: number | string | null): string | null {
		const numb = 0
		let response = ''
		// Test if thing is a number
		if (typeof number === 'string') {
			number = Number(number)
			if (isNaN(number)) throw new Error('arabicToRoman expects a number')
		}
		// Test other types
		if (typeof number !== 'number') throw new Error('arabicToRoman expects a number')
		if (isNaN(number)) throw new Error('arabicToRoman expects a real number')

		// Throw on negative number
		if (number < 0) throw new Error('arabicToRoman cannot express negative numbers')
		while (number > 0) {
			for (const numbKey in this.numbKeys) {
				if (this.numbKeys[numbKey] <= number) {
					response += numbKey
					number -= this.numbKeys[numbKey]
					break
				}
			}
		}
		return response
	}
}
