[numeralsconverter](README.md) › [Globals](globals.md)

# numeralsconverter

## Index

### External modules

* ["index"](modules/_index_.md)
* ["lib/apiTester"](modules/_lib_apitester_.md)
* ["lib/reader"](modules/_lib_reader_.md)
* ["server/server"](modules/_server_server_.md)
* ["services/arabicToRoman"](modules/_services_arabictoroman_.md)
* ["tests/api.spec"](modules/_tests_api_spec_.md)
* ["tests/arabicToRoman.spec"](modules/_tests_arabictoroman_spec_.md)
