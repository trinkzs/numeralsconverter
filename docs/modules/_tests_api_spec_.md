[numeralsconverter](../README.md) › [Globals](../globals.md) › ["tests/api.spec"](_tests_api_spec_.md)

# External module: "tests/api.spec"

## Index

### Variables

* [badTypes](_tests_api_spec_.md#const-badtypes)
* [numbers](_tests_api_spec_.md#const-numbers)

### Functions

* [useApi](_tests_api_spec_.md#useapi)

## Variables

### `Const` badTypes

• **badTypes**: *Array‹any›* =  ['string', {}, [], function(): void {}, -1]

Defined in tests/api.spec.ts:9

___

### `Const` numbers

• **numbers**: *Array‹number›* =  [1, 2, 3, 4, 5, 9, 10]

Defined in tests/api.spec.ts:8

API testing

## Functions

###  useApi

▸ **useApi**(`number`: number, `done`: CallableFunction): *void*

Defined in tests/api.spec.ts:15

Do post request and give result

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`number` | number | number to give to the api endpoint |
`done` | CallableFunction | callback  |

**Returns:** *void*
