[numeralsconverter](../README.md) › [Globals](../globals.md) › ["lib/apiTester"](_lib_apitester_.md)

# External module: "lib/apiTester"

## Index

### Functions

* [testApi](_lib_apitester_.md#testapi)

## Functions

###  testApi

▸ **testApi**(`number`: any): *void*

Defined in lib/apiTester.ts:11

Simple post that logs the api response or crash

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`number` | any | the arg we want to pass to the api endpoint  |

**Returns:** *void*
