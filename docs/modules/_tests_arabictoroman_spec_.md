[numeralsconverter](../README.md) › [Globals](../globals.md) › ["tests/arabicToRoman.spec"](_tests_arabictoroman_spec_.md)

# External module: "tests/arabicToRoman.spec"

## Index

### Variables

* [answers](_tests_arabictoroman_spec_.md#const-answers)
* [badTypes](_tests_arabictoroman_spec_.md#const-badtypes)
* [negNumbers](_tests_arabictoroman_spec_.md#const-negnumbers)
* [numberConverter](_tests_arabictoroman_spec_.md#const-numberconverter)
* [numbers](_tests_arabictoroman_spec_.md#const-numbers)
* [strNumbers](_tests_arabictoroman_spec_.md#const-strnumbers)

### Functions

* [doThrow](_tests_arabictoroman_spec_.md#dothrow)
* [dontThrow](_tests_arabictoroman_spec_.md#dontthrow)
* [testAnswer](_tests_arabictoroman_spec_.md#testanswer)

## Variables

### `Const` answers

• **answers**: *Array‹string›* =  ['I', 'II', 'III', 'IV', 'V', 'IX', 'X', 'XIV', 'XIX', 'XX', 'XXX', 'XXXI', 'XLIX', 'L', 'XCIX', 'C', 'CD', 'CM', 'M', 'MXXX', 'MCD', 'MM']

Defined in tests/arabicToRoman.spec.ts:13

___

### `Const` badTypes

• **badTypes**: *Array‹any›* =  ['string', {}, [], function(): void {}]

Defined in tests/arabicToRoman.spec.ts:12

___

### `Const` negNumbers

• **negNumbers**: *Array‹number›* =  numbers.map(x => x * -1)

Defined in tests/arabicToRoman.spec.ts:10

___

### `Const` numberConverter

• **numberConverter**: *[Converter](../classes/_services_arabictoroman_.converter.md)* =  new NumberConverter()

Defined in tests/arabicToRoman.spec.ts:8

Conversion algo testing

___

### `Const` numbers

• **numbers**: *Array‹number›* =  [1, 2, 3, 4, 5, 9, 10, 14, 19, 20, 30, 31, 49, 50, 99, 100, 400, 900, 1000, 1030, 1400, 2000]

Defined in tests/arabicToRoman.spec.ts:9

___

### `Const` strNumbers

• **strNumbers**: *Array‹string›* =  numbers.map(x => x.toString())

Defined in tests/arabicToRoman.spec.ts:11

## Functions

###  doThrow

▸ **doThrow**(`number`: number): *Func*

Defined in tests/arabicToRoman.spec.ts:19

Ensure that conversion throws error

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`number` | number |   |

**Returns:** *Func*

___

###  dontThrow

▸ **dontThrow**(`number`: any): *Func*

Defined in tests/arabicToRoman.spec.ts:31

Ensure that conversion doesnt throws error

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`number` | any |   |

**Returns:** *Func*

___

###  testAnswer

▸ **testAnswer**(`index`: any): *(Anonymous function)*

Defined in tests/arabicToRoman.spec.ts:43

Ensure that conversion algo give the right conversion

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`index` | any |   |

**Returns:** *(Anonymous function)*
