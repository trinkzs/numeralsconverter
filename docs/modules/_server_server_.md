[numeralsconverter](../README.md) › [Globals](../globals.md) › ["server/server"](_server_server_.md)

# External module: "server/server"

## Index

### Variables

* [app](_server_server_.md#const-app)
* [numberConverter](_server_server_.md#const-numberconverter)

## Variables

### `Const` app

• **app**: *Express* =  express()

Defined in server/server.ts:5

___

### `Const` numberConverter

• **numberConverter**: *[Converter](../classes/_services_arabictoroman_.converter.md)* =  new NumberConverter()

Defined in server/server.ts:4
