[numeralsconverter](../README.md) › [Globals](../globals.md) › ["index"](_index_.md)

# External module: "index"

## Index

### Variables

* [numberConverter](_index_.md#const-numberconverter)

### Functions

* [checkAndLog](_index_.md#checkandlog)

## Variables

### `Const` numberConverter

• **numberConverter**: *[Converter](../classes/_services_arabictoroman_.converter.md)* =  new NumberConverter()

Defined in index.ts:5

## Functions

###  checkAndLog

▸ **checkAndLog**(`arg`: any): *void*

Defined in index.ts:18

**Parameters:**

Name | Type |
------ | ------ |
`arg` | any |

**Returns:** *void*
