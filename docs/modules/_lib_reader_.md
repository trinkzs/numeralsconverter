[numeralsconverter](../README.md) › [Globals](../globals.md) › ["lib/reader"](_lib_reader_.md)

# External module: "lib/reader"

## Index

### Variables

* [readFileP](_lib_reader_.md#const-readfilep)

### Functions

* [readConfigFile](_lib_reader_.md#readconfigfile)

## Variables

### `Const` readFileP

• **readFileP**: *__promisify__* =  util.promisify(fs.readFile)

Defined in lib/reader.ts:3

## Functions

###  readConfigFile

▸ **readConfigFile**(`path?`: undefined | string): *Promise‹any›*

Defined in lib/reader.ts:9

Read /src/config/config.json and get number property from this file

**Parameters:**

Name | Type |
------ | ------ |
`path?` | undefined &#124; string |

**Returns:** *Promise‹any›*

Promise<any>
