[numeralsconverter](README.md) › [Globals](globals.md)

# numeralsconverter

# numeralsConverter

This project convert your arabic numerals to roman numerals.

## Requirements
- NodeJS 8+
- Typescript => `npm i -g typescript`
- ts-node => `npm i -g ts-node`

## Installation
`npm i`
<br>(packages traking are managed by the `package-lock.json` file)

## Configuration
If you want to use the file option of the converter :
- Set the value that you want in src/config/config.json

### Folders
- `src`: manage the code of the application
  - `config/`: The confif files for the script
  - `lib/`: Utils for testing or other little tasks
    - `server/`: express app 
  - `services/`: Conversion scripts
  - `tests/`: All tests executed with mocha
  - `index.ts`: Entry file of the application

## Using the application

**Command Line Number**
---
  Launch the conversion script with a command line argument

* **Command**
	```ts-node src/index.ts -c <<your number>>```

**Command line array of numbers**
---
  Launch the conversion script with a command line argument

* **Command**
	```ts-node src/index.ts -A <<Array[number]>> => ts-node src/index.ts -A [1,2] ```

**File reading conversion**
---
  Launch the conversion script with an argument passed to config.json file

* **Command**
	```ts-node src/index.ts -f```

	You can also give a custom file path to read

* **Command**
	```ts-node src/index.ts -F <<your path>>```

**Conversion API**
----
  Returns roman conversion of arabic numeral

* **URL**

  localhost:3000/arabicToRoman/

* **Method:**

  `POST`
  

* **Data Params**

  ```
{
	number: string | number =>  the number your want to convert
}
```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `XIII`
 
* **Error Response:**

  * **Code:** 500 internal server error <br />
    **Content:** `Bad parameter`, give precisions about the error
