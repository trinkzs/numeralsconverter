[numeralsconverter](../README.md) › [Globals](../globals.md) › ["services/arabicToRoman"](../modules/_services_arabictoroman_.md) › [Converter](_services_arabictoroman_.converter.md)

# Class: Converter

## Hierarchy

* **Converter**

## Index

### Constructors

* [constructor](_services_arabictoroman_.converter.md#constructor)

### Properties

* [numbKeys](_services_arabictoroman_.converter.md#protected-numbkeys)

### Methods

* [arabicToRoman](_services_arabictoroman_.converter.md#arabictoroman)

## Constructors

###  constructor

\+ **new Converter**(): *[Converter](_services_arabictoroman_.converter.md)*

Defined in services/arabicToRoman.ts:5

**Returns:** *[Converter](_services_arabictoroman_.converter.md)*

## Properties

### `Protected` numbKeys

• **numbKeys**: *any*

Defined in services/arabicToRoman.ts:5

## Methods

###  arabicToRoman

▸ **arabicToRoman**(`number`: number | string | null): *string | null*

Defined in services/arabicToRoman.ts:30

Convert given number to roman conversion

**Parameters:**

Name | Type |
------ | ------ |
`number` | number &#124; string &#124; null |

**Returns:** *string | null*

string Complete information of the cinema or void if error
